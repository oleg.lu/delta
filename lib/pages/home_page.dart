import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../delta_consts.dart';
import '../list_data.dart';
import '../widgets/new_card.dart';
import '../widgets/new_card_inside.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<ListData>(builder: ((context, value, child) {
      return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
             actions: [
              Expanded(
                child: TabBar(
                  tabs: [
                    Tab(icon: Icon(Icons.line_weight_sharp)),
                    Tab(icon: Icon(Icons.book_outlined)),
                  ],
                ),
              ),
             ]
            ),
            body: TabBarView(
              children: [
                ListView.builder(
                  itemCount: DeltaConsts.titleList.length,
                  itemBuilder: (context, index) {
                    return NewCard(
                      title: DeltaConsts.titleList[index],
                      imagePath: DeltaConsts.imgList[index],
                      index: index,
                    );
                  },
                ),
                ListView.builder(
                  itemCount: context.watch<ListData>().getList.length,
                  itemBuilder: (context, index) {
                    return MyCardInside(
                      title: DeltaConsts.subTitleList[
                          Provider.of<ListData>(context, listen: false).getList[index]],
                      imagePath: DeltaConsts
                          .imgList[context.watch<ListData>().getList[index]],
                      index: Provider.of<ListData>(context, listen: false).getList[index],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
    }));
  }
}
