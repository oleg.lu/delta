import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../ds/shared.dart';

class WebViewPage extends StatefulWidget {
  final String rem;
  const WebViewPage({super.key, required this.rem});

  @override
  State<WebViewPage> createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  WebViewController? _controller;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (await _controller!.canGoBack()) {
          _controller!.goBack();
        }
        return false;
      },
      child: Scaffold(
        body: WebView(
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller = webViewController;
          },
          initialUrl: widget.rem,
          
        ),
      ),
    );
  }
}