import 'package:flutter/material.dart';

class ListData with ChangeNotifier{
  Set<int> list = {};
  
  List get getList => list.toList();
  
  changeList (int index1){
    list.add(index1);
    notifyListeners();
  }

  popList (int index1){
    list.remove(index1);
    notifyListeners();
  }
}