import 'package:shared_preferences/shared_preferences.dart';


class Shared {
   static  getPath() async {
    SharedPreferences storage = await SharedPreferences.getInstance();
   return  storage.getString('urlKey')?? '';
  }

  static setPath(path1) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    final path = storage.setString('urlKey', path1);
    print(path);
  }

}