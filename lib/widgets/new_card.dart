import 'package:delta_soft/list_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewCard extends StatelessWidget {
  final String title;
  final String imagePath;
  final int index;
  

  const NewCard(
      {super.key,
      required this.title,
      required this.imagePath,
      required this.index,
      });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ListData>(
      create: (context) => ListData(),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
                image: AssetImage(
                  imagePath,
                ),
                fit: BoxFit.cover),
          ),
          height: 230,
          width: double.infinity,
          child: SizedBox(
            width: 230,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // const SizedBox(
                //   height: 10,
                // ),
                const Text('SPORT NEWS',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                        shadows: [
                          Shadow(
                              // bottomLeft
                              offset: Offset(-1, -1),
                              color: Colors.black),
                          Shadow(
                              // bottomRight
                              offset: Offset(1, -1),
                              color: Colors.black),
                          Shadow(
                              // topRight
                              offset: Offset(1, 1),
                              color: Colors.black),
                          Shadow(
                              // topLeft
                              offset: Offset(-1, 1),
                              color: Colors.black),
                        ])),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    title,
                    style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Abbieshire',
                        color: Colors.blueAccent,
                        shadows: [
                          Shadow(
                              // bottomLeft
                              offset: Offset(-1, -1),
                              color: Colors.black),
                          Shadow(
                              // bottomRight
                              offset: Offset(1, -1),
                              color: Colors.black),
                          Shadow(
                              // topRight
                              offset: Offset(1, 1),
                              color: Colors.black),
                          Shadow(
                              // topLeft
                              offset: Offset(-1, 1),
                              color: Colors.black),
                        ]),
                  ),
                ),
                GestureDetector(
                onTap: () => {
                  Provider.of<ListData>(context, listen: false).changeList(index)
                },
                child: CircleAvatar(
                  child: Icon(
                    Icons.add,
                    size: 24,
                  ),
                ),
              )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
