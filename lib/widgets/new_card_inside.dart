import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../list_data.dart';

class MyCardInside extends StatelessWidget {
  final String imagePath;
  final String title;
  final int index;
  const MyCardInside(
      {super.key,
      required this.title,
      required this.imagePath,
      required this.index});

  @override
  Widget build(BuildContext context) {
    return Consumer<ListData>(builder: (context, value, child) {
      return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        height: 300,
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          image: DecorationImage(
              image: AssetImage(
                imagePath,
              ),
              fit: BoxFit.fill),
        ),
        child: GestureDetector(
          onTap: () =>
              {Provider.of<ListData>(context, listen: false).popList(index)},
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                  padding: EdgeInsets.all(20),
                  width: 300,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromARGB(149, 63, 61, 61)),
                  child: Text(
                    title,
                    style: TextStyle(color: Colors.white),
                  )),
              Container(
                height: 50,
                child: CircleAvatar(
                  radius: 50,
                  child: Icon(
                    Icons.remove,
                    size: 24,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
