import 'package:delta_soft/ds/shared.dart';
import 'package:delta_soft/pages/home_page.dart';
import 'package:delta_soft/pages/web_view_page.dart';
import 'package:delta_soft/start.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'list_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  String startCalback = await Start().start();
  String getLink = await Shared.getPath();
  runApp(getLink == '' ? MyApp(startCalback: startCalback) : 
  MaterialApp(
    debugShowCheckedModeBanner: false,
    home: WebViewPage(rem: getLink)));
}

class MyApp extends StatelessWidget {
  String startCalback;
  MyApp({super.key, required this.startCalback});

  

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ListData())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: startCalback == '' ? HomePage(): WebViewPage(rem: startCalback)),
    );
  }
}
